//
//  ViewController.m
//  PowerMeter_Animation
//
//  Created by Admin on 1/5/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ViewController.h"
#import "ActivityRoundChart.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *animationScene;
@property (weak, nonatomic) IBOutlet UIView *animationScene2;
@property (weak, nonatomic) IBOutlet ActivityRoundChart *roundChartView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    UIImage *img = [UIImage animatedImageNamed:@"animation-" duration:0.7f];
//    [self.imageView setImage:img];
    [self showAnimationWithContainer:self.animationScene];
    
    [self showAnimationWithContainer:self.animationScene2];
    
    self.roundChartView.backgroungColor = [UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1];
    self.roundChartView.fillColor = [UIColor colorWithRed:0 green:138./255. blue:190./255. alpha:1];
    [self.roundChartView setValue:75 animated:YES];
    
}

- (void)viewDidAppear:(BOOL)animated {
}

- (void)showAnimationWithContainer:(UIView *)container {

    NSMutableArray *layersArray = [@[] mutableCopy];
    
    int circleCount = 4;
    float startRadius = 20;
    
    float delta = (container.frame.size.width - startRadius * 2) / (circleCount * 2);
    CGPoint center = CGPointMake(container.frame.size.width / 2, container.frame.size.height / 2);
    
    for (int i = 1; i <= circleCount; i++) {
        CALayer *layer = [CALayer layer];
        [container.layer addSublayer:layer];
        layer.borderWidth = 2;
        layer.borderColor = [UIColor colorWithRed:41./255. green:171./255. blue:226./255. alpha:1].CGColor;
        layer.frame = CGRectMake(center.x - delta * i - startRadius, center.y - delta * i - startRadius, delta * i * 2 + startRadius * 2, delta * i * 2 + startRadius * 2);
        layer.cornerRadius = layer.bounds.size.width / 2;
        [layersArray addObject:layer];
    }
    
    
    for (CALayer *layer in layersArray) {
        CABasicAnimation *boundsAnimation = [CABasicAnimation animationWithKeyPath:@"bounds.size"];
        boundsAnimation.fromValue = [NSValue valueWithCGSize:layer.bounds.size];
        boundsAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(layer.bounds.size.width + delta * 2, layer.bounds.size.height + delta * 2)];
        
        CABasicAnimation *cornerAnimation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
        cornerAnimation.fromValue = @(layer.bounds.size.width / 2);
        cornerAnimation.toValue = @((layer.bounds.size.width + delta * 2) / 2);

        CABasicAnimation *colorAnimation = [CABasicAnimation animationWithKeyPath:@"borderColor"];
        float startAlpha = 1 - ((float)[layersArray indexOfObject:layer]) / (float)layersArray.count;
        if (startAlpha == 1) {
            startAlpha = 0;
        }

        float endAlpha = 1 - (float)([layersArray indexOfObject:layer] + 1) / (float)layersArray.count;
        colorAnimation.fromValue = (id)[UIColor colorWithRed:41./255. green:171./255. blue:226./255. alpha:startAlpha].CGColor;
        colorAnimation.toValue = (id)[UIColor colorWithRed:41./255. green:171./255. blue:226./255. alpha:endAlpha].CGColor;
        
        
        CABasicAnimation *widthAnimation = [CABasicAnimation animationWithKeyPath:@"borderWidth"];
        widthAnimation.fromValue = @(4. / (float)layersArray.count * ((float)[layersArray indexOfObject:layer]));
        widthAnimation.toValue = @(4. / (float)layersArray.count * ((float)[layersArray indexOfObject:layer] + 1));
        
        
        CAAnimationGroup *group = [CAAnimationGroup animation];
        group.duration = 0.5;
        group.repeatCount = 1000000;
        group.autoreverses = NO;
        //group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        group.animations = @[boundsAnimation, cornerAnimation, colorAnimation, widthAnimation];
        
        [layer addAnimation:group forKey:@"allMyAnimations"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
