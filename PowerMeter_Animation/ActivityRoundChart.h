//
//  ActivityRoundChart.h
//  PowerMeter_Animation
//
//  Created by Admin on 1/9/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityRoundChart : UIView

- (void)setValue:(CGFloat)value animated:(BOOL)animated;

@property (nonatomic,strong) UIColor *backgroungColor;
@property (nonatomic,strong) UIColor *fillColor;

@end
