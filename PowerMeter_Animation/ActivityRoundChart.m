//
//  ActivityRoundChart.m
//  PowerMeter_Animation
//
//  Created by Admin on 1/9/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ActivityRoundChart.h"

@interface ActivityRoundChart () {
    CAShapeLayer *_bgLayer;
    CAShapeLayer *_progressLayer;
    CGFloat _value;
}

@end

@implementation ActivityRoundChart


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}


- (void)setup {

    float lineWidth = 10;
    
    _bgLayer = [CAShapeLayer layer];
    _bgLayer.frame = self.layer.bounds;
    _bgLayer.fillColor = [UIColor clearColor].CGColor;
    _bgLayer.strokeColor = [UIColor grayColor].CGColor;
    _bgLayer.lineWidth = lineWidth;
    _bgLayer.lineCap = kCALineCapRound;
    _bgLayer.path = [self pathForLine].CGPath;
    [self.layer addSublayer:_bgLayer];
    
    _progressLayer = [CAShapeLayer layer];
    _progressLayer.frame = self.layer.bounds;
    _progressLayer.fillColor = [UIColor clearColor].CGColor;
    _progressLayer.strokeColor = [UIColor blueColor].CGColor;
    _progressLayer.lineWidth = lineWidth;
    _progressLayer.lineCap = kCALineCapRound;
    _progressLayer.path = [self pathForLine].CGPath;
    [self.layer addSublayer:_progressLayer];
    
}


- (UIBezierPath *)pathForLine {
    CGFloat radius = (MIN(self.bounds.size.width, self.bounds.size.height) - 10.f) / 2.f;
    CGPoint centerPoint = CGPointMake(self.bounds.size.width / 2.f, self.bounds.size.height / 2.f);
    
    return [UIBezierPath bezierPathWithArcCenter:centerPoint
                                          radius:radius
                                      startAngle:-M_PI_2
                                        endAngle:-M_PI_2 + 2 * M_PI
                                       clockwise:YES];
}

#pragma mark - Value property

- (void)setValue:(CGFloat)value animated:(BOOL)animated {
    _value = value;
    [self updatePathAnimated:animated];
}

- (void)setBackgroungColor:(UIColor *)backgroungColor {
    _backgroungColor = backgroungColor;
    _bgLayer.strokeColor = backgroungColor.CGColor;
    
}

- (void)setFillColor:(UIColor *)fillColor {
    _fillColor = fillColor;
    _progressLayer.strokeColor = fillColor.CGColor;
}

- (void)updatePathAnimated:(BOOL)animated {
    
    _progressLayer.strokeStart = 0;
    _progressLayer.strokeEnd = _value / 100.;
    if (animated) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        animation.duration = 2.f;
        animation.fromValue = @(0);
        animation.toValue = @(_value / 100.);
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        [_progressLayer addAnimation:animation forKey:nil];
    }
    else {
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        _progressLayer.strokeEnd = _value / 100.;
        [CATransaction commit];
    }
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    _progressLayer.frame = self.layer.bounds;
    _bgLayer.frame = self.layer.bounds;
}

@end
